require 'test_helper'

class SubstatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @substate = substates(:one)
  end

  test "should get index" do
    get substates_url
    assert_response :success
  end

  test "should get new" do
    get new_substate_url
    assert_response :success
  end

  test "should create substate" do
    assert_difference('Substate.count') do
      post substates_url, params: { substate: { name: @substate.name, project_id: @substate.project_id, state_id: @substate.state_id } }
    end

    assert_redirected_to substate_url(Substate.last)
  end

  test "should show substate" do
    get substate_url(@substate)
    assert_response :success
  end

  test "should get edit" do
    get edit_substate_url(@substate)
    assert_response :success
  end

  test "should update substate" do
    patch substate_url(@substate), params: { substate: { name: @substate.name, project_id: @substate.project_id, state_id: @substate.state_id } }
    assert_redirected_to substate_url(@substate)
  end

  test "should destroy substate" do
    assert_difference('Substate.count', -1) do
      delete substate_url(@substate)
    end

    assert_redirected_to substates_url
  end
end
