Rails.application.routes.draw do

  resources :substates
  resources :constituencies
  resources :states do
    #resources :constituencies do
    #  resources :projects
    #end
  end

  root 'home#index'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
