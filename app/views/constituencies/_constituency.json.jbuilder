json.extract! constituency, :id, :name, :state_id, :project_id, :created_at, :updated_at
json.url constituency_url(constituency, format: :json)
