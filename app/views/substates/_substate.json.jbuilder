json.extract! substate, :id, :name, :state_id, :project_id, :created_at, :updated_at
json.url substate_url(substate, format: :json)
