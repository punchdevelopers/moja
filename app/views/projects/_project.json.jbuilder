json.extract! project, :id, :name, :description, :level, :cost, :contractor, :created_at, :updated_at
json.url project_url(project, format: :json)
