class SubstatesController < ApplicationController
  before_action :set_substate, only: [:show, :edit, :update, :destroy]

  # GET /substates
  # GET /substates.json
  def index
    @substates = Substate.all
  end

  # GET /substates/1
  # GET /substates/1.json
  def show
  end

  # GET /substates/new
  def new
    @substate = Substate.new
  end

  # GET /substates/1/edit
  def edit
  end

  # POST /substates
  # POST /substates.json
  def create
    @substate = Substate.new(substate_params)

    respond_to do |format|
      if @substate.save
        format.html { redirect_to @substate, notice: 'Substate was successfully created.' }
        format.json { render :show, status: :created, location: @substate }
      else
        format.html { render :new }
        format.json { render json: @substate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /substates/1
  # PATCH/PUT /substates/1.json
  def update
    respond_to do |format|
      if @substate.update(substate_params)
        format.html { redirect_to @substate, notice: 'Substate was successfully updated.' }
        format.json { render :show, status: :ok, location: @substate }
      else
        format.html { render :edit }
        format.json { render json: @substate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /substates/1
  # DELETE /substates/1.json
  def destroy
    @substate.destroy
    respond_to do |format|
      format.html { redirect_to substates_url, notice: 'Substate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_substate
      @substate = Substate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def substate_params
      params.require(:substate).permit(:name, :state_id, :project_id)
    end
end
