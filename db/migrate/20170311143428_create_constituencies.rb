class CreateConstituencies < ActiveRecord::Migration[5.0]
  def change
    create_table :constituencies do |t|
      t.string :title
      t.integer :state_id
      t.integer :project_id

      t.timestamps
    end
  end
end
