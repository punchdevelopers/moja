class CreateSubstates < ActiveRecord::Migration[5.0]
  def change
    create_table :substates do |t|
      t.string :name
      t.integer :state_id
      t.integer :project_id

      t.timestamps
    end
  end
end
